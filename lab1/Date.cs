﻿namespace lab1
{
    public struct Date // структура для описания даты день:месяц:год
    {
        public int Day { get; }
        public int Month { get; }
        public int Year { get; }

        public Date(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
        }

        public override string ToString()
        {
            return $"{Day:00}.{Month:00}.{Year:00}";
        }
    }
}

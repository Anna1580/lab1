﻿namespace lab1
{
    public struct PhoneCalls // структура для описания данных о звонках
    {
        public string PhoneNumber { get; }
        public Date Date { get; }
        public Time StartTime { get; }
        public Time Duration { get; }
        public Rate Rate { get; }
        public double Cost { get; }

        public PhoneCalls(string phoneNumber, Date date, Time startTime, Time duration, Rate rate, double cost)
        {
            PhoneNumber = phoneNumber;
            Date = date;
            StartTime = startTime;
            Duration = duration;
            Rate = rate;
            Cost = cost;
        }

        public override string ToString()
        {
            return $"{PhoneNumber} {Date} {StartTime} {Duration} {Rate} {Cost}";
        }
    }
}

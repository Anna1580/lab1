﻿using System;
using System.Collections.Generic;
using System.IO;

namespace lab1
{
    public class Processing // класс для обработки данных
    {
        public List<PhoneCalls> Calls { get; }

        public Processing()
        {
            Calls = new List<PhoneCalls>();
        }

        // чтение данных из файла
        public void ReadFromFile(string filePath)
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine(); // читаем строку
                    string[] elements = line.Split(" "); // разбиваем строку по пробелам
                    string phoneNumber = elements[0];
                    string[] substr = elements[1].Split("."); // разбиваем подстроку для даты
                    Date date = new Date(Convert.ToInt32(substr[0]), Convert.ToInt32(substr[1]), Convert.ToInt32(substr[2])); // дата звонка
                    substr = elements[2].Split(":"); // снова разбиваем подстроку для времени
                    Time startTime = new Time(Convert.ToInt32(substr[0]), Convert.ToInt32(substr[1]), Convert.ToInt32(substr[2])); // время начала звонка
                    substr = elements[3].Split(":"); // снова разбиваем подстроку для времени
                    Time duration = new Time(Convert.ToInt32(substr[0]), Convert.ToInt32(substr[1]), Convert.ToInt32(substr[2]));
                    Rate rate = (Rate)Enum.Parse(typeof(Rate), elements[4], true); // преобразование текста к элементу перечисления
                    double cost = double.Parse(elements[5].Replace('.', ',')); // преобразование в дробное число
                    Calls.Add(new PhoneCalls(phoneNumber, date, startTime, duration, rate, cost)); // сгружаем все в структуру

                }
            }
        }

        // вывод переданного в параметре списка в консоль
        public void Print(List<PhoneCalls> calls)
        {
            foreach (PhoneCalls call in calls)
            {
                Console.WriteLine(call.ToString());
            }
            Console.WriteLine();
        }

        // вывод всех разговоров на мобильных
        public List<PhoneCalls> PrintAllMobileCalls()
        {
            List<PhoneCalls> mobileCalls = new List<PhoneCalls>();
            foreach(PhoneCalls call in Calls)
            {
                if (call.Rate == Rate.Мобильный)
                {
                    mobileCalls.Add(call);
                }
            }
            return mobileCalls;
        }

        // вывод всех разговоров за ноябрь 2021
        public List<PhoneCalls> PrintAllCallsByDate()
        {
            List<PhoneCalls> mobileCalls = new List<PhoneCalls>();
            foreach (PhoneCalls call in Calls)
            {
                if (call.Date.Year == 21 && call.Date.Month == 11)
                {
                    mobileCalls.Add(call);
                }
            }
            return mobileCalls;
        }

        public double AverageCostOfAllCalls()
        {
            int totalDuration = 0;
            double totalCost = 0;
            foreach(PhoneCalls call in Calls)
            {
                totalCost += call.Duration.TotalSeconds() * (call.Cost / 60); // умножаем количество секунд звонка на стоимость секунды звонка
                totalDuration += call.Duration.TotalSeconds(); // считаем общее количество секунд разговоров
            }

            return totalCost/totalDuration; // делим общее время разговоров на сумму всех разговоров (все в секундах)
        }

        // отправная точка пирамидальной сортировки
        public List<PhoneCalls> SortListWithPyramidSort(List<PhoneCalls> list, SortingParameter param)
        {
            List<PhoneCalls> calls = new List<PhoneCalls>();
            foreach (PhoneCalls call in list)
            {
                calls.Add(call);
            }

            switch (param) // в зависимости от параметра вызываем саму сортировку
            {
                case SortingParameter.ByDurationDesc:
                    return PyramidSort(calls, calls.Count, SortingParameter.ByDurationDesc);
                case SortingParameter.ByPhoneNumberAsc:
                    calls = PyramidSort(calls, calls.Count, SortingParameter.ByCostDesc);
                    return PyramidSort(calls, calls.Count, SortingParameter.ByPhoneNumberAsc);
                default: return list;
            }
        }

        // поиск индексов для построения пирамиды
        private int AddToPyramid(List<PhoneCalls> list, int i, int N, SortingParameter param)
        {
            int imax = 0;
            if ((2 * i + 2) < N)
            {
                switch (param)
                {
                    case SortingParameter.ByDurationDesc:
                        if (list[2 * i + 1].Duration.TotalSeconds() > list[2 * i + 2].Duration.TotalSeconds())
                        {
                            imax = 2 * i + 2;
                        }
                        else
                        {
                            imax = 2 * i + 1;
                        }

                        break;
                    case SortingParameter.ByCostDesc:
                        if (list[2 * i + 1].Cost > list[2 * i + 2].Cost)
                        {
                            imax = 2 * i + 2;
                        }
                        else
                        {
                            imax = 2 * i + 1;
                        }

                        break;
                    case SortingParameter.ByPhoneNumberAsc:
                        if (list[2 * i + 1].PhoneNumber.CompareTo(list[2 * i + 2].PhoneNumber) < 0)
                        {
                            imax = 2 * i + 2;
                        }
                        else
                        {
                            imax = 2 * i + 1;
                        }

                        break;
                }
            }
            else
            {
                imax = 2 * i + 1;
            }

            if (imax >= N)
            {
                return i;
            }

            switch (param)
            {
                case SortingParameter.ByDurationDesc:
                    if (list[i].Duration.TotalSeconds() > list[imax].Duration.TotalSeconds())
                    {
                        (list[i], list[imax]) = (list[imax], list[i]);
                        if (imax < N / 2)
                        {
                            i = imax;
                        }
                    }

                    break;
                case SortingParameter.ByCostDesc:
                    if (list[i].Cost > list[imax].Cost)
                    {
                        (list[i], list[imax]) = (list[imax], list[i]);
                        if (imax < N / 2)
                        {
                            i = imax;
                        }
                    }

                    break;
                case SortingParameter.ByPhoneNumberAsc:
                    if (list[i].PhoneNumber.CompareTo(list[imax].PhoneNumber) < 0)
                    {
                        (list[i], list[imax]) = (list[imax], list[i]);
                        if (imax < N / 2)
                        {
                            i = imax;
                        }
                    }

                    break;
            }

            return i;
        }

        private List<PhoneCalls> PyramidSort(List<PhoneCalls> list, int len, SortingParameter param)
        {
            // построение пирамиды
            for (int i = len / 2 - 1; i >= 0; --i)
            {
                int prev_i = i;
                i = AddToPyramid(list, i, len, param);
                if (prev_i != i)
                {
                    ++i;
                }
            }

            // сортировка
            for (int k = len - 1; k > 0; --k)
            {
                (list[0], list[k]) = (list[k], list[0]);
                int i = 0, prev_i = -1;
                while (i != prev_i)
                {
                    prev_i = i;
                    i = AddToPyramid(list, i, k, param);
                }
            }

            return list;
        }

        // отправная точка пирамидальной сортировки
        public List<PhoneCalls> SortListWithQuickSort(List<PhoneCalls> list, SortingParameter param)
        {
            List<PhoneCalls> calls = new List<PhoneCalls>();
            foreach (PhoneCalls call in list)
            {
                calls.Add(call);
            }

            switch (param) // в зависимости от параметра вызываем саму сортировку
            {
                case SortingParameter.ByDurationDesc:
                    QuickSort(calls, 0, calls.Count - 1, SortingParameter.ByDurationDesc);
                    break;
                case SortingParameter.ByPhoneNumberAsc:
                    QuickSort(calls, 0, calls.Count - 1, SortingParameter.ByCostDesc);
                    QuickSort(calls, 0, calls.Count - 1, SortingParameter.ByPhoneNumberAsc);
                    break;
                default: return list;
            }

            return calls;
        }

        // разбитие всего списка на разделы
        private int Partition(List<PhoneCalls> list, int start, int end, SortingParameter param)
        {
            int marker = start;
            for (int i = start; i < end; i++)
            {
                switch (param)
                {
                    case SortingParameter.ByDurationDesc:
                        if (list[i].Duration.TotalSeconds() > list[end].Duration.TotalSeconds())
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                    case SortingParameter.ByPhoneNumberAsc:
                        if (list[i].PhoneNumber.CompareTo(list[end].PhoneNumber) < 0)
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                    case SortingParameter.ByCostDesc:
                        if (list[i].Cost > list[end].Cost)
                        {
                            (list[marker], list[i]) = (list[i], list[marker]);
                            marker += 1;
                        }
                        break;
                }
            }

            (list[marker], list[end]) = (list[end], list[marker]);
            return marker;
        }

        // сортировка разделов
        private void QuickSort(List<PhoneCalls> list, int start, int end, SortingParameter param)
        {
            if (start >= end)
            {
                return;
            }

            int pivot = Partition(list, start, end, param);
            QuickSort(list, start, pivot - 1, param);
            QuickSort(list, pivot + 1, end, param);

        }
    }
}

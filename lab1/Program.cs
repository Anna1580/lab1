﻿using System;

namespace lab1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Laboratory work #1. Git");
            Console.WriteLine("Variant #9. Phone talks");
            Console.WriteLine("Author: Anna Rybakova");
            Console.WriteLine("Group: 22POIT1z");

            Processing proc = new Processing();
            proc.ReadFromFile(@"..\..\..\data.txt");

            proc.Print(proc.Calls);

            Console.WriteLine("Все звонки на мобильный:");
            proc.Print(proc.PrintAllMobileCalls());
            Console.WriteLine("Все звонки за ноябрь 2021:");
            proc.Print(proc.PrintAllCallsByDate());

            Console.WriteLine("Пирамидальная сортировка по убыванию длительности звонка:");
            proc.Print(proc.SortListWithPyramidSort(proc.Calls, SortingParameter.ByDurationDesc));
            Console.WriteLine("Пирамидальная сортировка по возрастанию номера телефона, а в рамках одного номера по убыванию стоимости разговора:");
            proc.Print(proc.SortListWithPyramidSort(proc.Calls, SortingParameter.ByPhoneNumberAsc));

            Console.WriteLine("Быстрая сортировка по убыванию длительности звонка:");
            proc.Print(proc.SortListWithQuickSort(proc.Calls, SortingParameter.ByDurationDesc));
            Console.WriteLine("Быстрая сортировка по возрастанию номера телефона, а в рамках одного номера по убыванию стоимости разговора:");
            proc.Print(proc.SortListWithQuickSort(proc.Calls, SortingParameter.ByPhoneNumberAsc));

            Console.WriteLine($"Средняя стоимость секунды разговора: {Math.Round(proc.AverageCostOfAllCalls(), 3)}");
        }
    }
}

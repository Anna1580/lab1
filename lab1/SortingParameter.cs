﻿namespace lab1
{
    public enum SortingParameter
    {
        ByDurationDesc,
        ByPhoneNumberAsc,
        ByCostDesc
    }
}

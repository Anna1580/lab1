﻿namespace lab1
{
    public struct Time // стркутура для описания времени часы:минуты:секунды
    {
        public int Hours { get; }
        public int Minutes { get; }
        public int Seconds { get; }

        public Time(int hours, int minutes, int seconds)
        {
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
        }

        public override string ToString()
        {
            return $"{Hours:00}:{Minutes:00}:{Seconds:00}";
        }

        // считает общее количество секунд
        public int TotalSeconds()
        {
            return Hours*3600 + Minutes*60 + Seconds;
        }

    }
}
